(function (root, declare) {
    if (
        typeof define === 'function' && define.amd
    ) define(['exports'], declare);
    else if (
        typeof module === 'object' &&
        typeof module.exports === 'object'
    ) declare(module.exports);
    else declare(root.fxloop = Object.create(null));
})(typeof self !== 'undefined'?self:global, function (exports) {

    const nope = () => { };

    let render = nope;
    let update = nope;
    let simulate = nope;

    let timestep = 16.666666666666668;
    let timestep_s = 0.016666666666666666;

    Object.defineProperties(exports, {
        render: {
            get() { return getCB(render); },
            set(value) { render = setCB(render, value); }
        },
        update: {
            get() { return getCB(update); },
            set(value) { update = setCB(update, value); }
        },
        simulate: {
            get() { return getCB(simulate); },
            set(value) { simulate = setCB(simulate, value); }
        },
        timestep: {
            get() { return timestep; },
            set(value) {
                timestep = value;
                timestep_s = value / 1000;
            }
        },
        ups: {
            get() { return 1 / timestep_s; },
            set(value) {
                timestep = 1000 / value;
                timestep_s = 1 / value;
            }
        }
    });

    let id = 0;
    let last_t = 0;
    let unsimulated = 0;
    let rerender = true;
    let updated = false;

    function frame() {

        id = requestAnimationFrame(frame);

        const t = performance.now();
        const elapsed = t - last_t;

        rerender = false;

        unsimulated += elapsed;

        if (unsimulated > 300)
            unsimulated = timestep;

        updated = update(elapsed / 1000);

        while (unsimulated >= timestep) {
            simulate(timestep_s);
            unsimulated -= timestep;
            rerender = true;
        }

        if (rerender || updated)
            render(unsimulated / timestep);

        last_t = t;

    }

    exports.stop = function stop() {
        cancelAnimationFrame(id);
    };

    exports.run = function run() {
        unsimulated = 0;
        rerender = true;
        last_t = performance.now();
        id = requestAnimationFrame(frame);
    };

    function getCB(value) {
        return value === nope ? null : value;
    }

    function setCB(old, value) {
        if (value == null) return nope;
        if (typeof value === 'function')
            return value;
        return old;
    }

})